
>[!warning]
> This project is till WIP. Please use with caution.


# Description

A simple shell (**sh**) that is capable of interacting with qemu/KVM/Libvirt virtual machines be it Linux or Windows (BSD not tested yet, but it should work) and perform file operations.

This shell script can do the following tasks:
    - List virtual machine partitions
    - List VM files under a specified partition
    - Copy files to a specified VM partition
    - Copy files from a specified VM partition to a host directory

# Dependencies
    - `libguestfs`
    - `guestfish`

# Usage

Just run the script without any arguments.

